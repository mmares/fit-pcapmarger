﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PcapMerger
{
    interface IPmFrame : IComparable
    {

        /// <summary>
        /// Return type of the frame by originating file (Pcap, PcapNg, MNM)
        /// </summary>
        PmSupportedTypes.CaputreFileFrameType Type();

        /// <summary>
        /// Interface is IComparable
        /// </summary>
        int CompareTo(IPmFrame other);

        /// <summary>
        /// Return RAW frame data
        /// </summary>
        byte[] Data();

        /// <summary>
        /// Return link type of the frame
        /// </summary>
        PmSupportedTypes.LinkTypes LinkType();

        /// <summary>
        /// Return frame capture time stamp
        /// </summary>
        DateTime TimeStamp();

        /// <summary>
        /// Packet original length
        /// </summary>
        UInt32 OriginalLength();

        /// <summary>
        /// Size of the captured data
        /// </summary>
        UInt32 IncludedLength();
        

        /// <summary>
        /// Frame offset in original file
        /// </summary>
        UInt32 FrameOffset();
    }
}
