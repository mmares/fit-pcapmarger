﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2012 Vladimir Vesely (Brno University of Technology - Faculty of information technology)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace PcapMerger
{
    /// <remarks>
    /// Interface that must be implemented by all supproted file types classes
    /// </remarks>
    internal interface IPmCaptureFile
    {

        #region OutputMethods

        /// <summary>
        /// Returns instance type
        /// </summary>
        PmSupportedTypes.CaptureFileTypes FileType();

        /// <summary>
        /// Returns file name of file from which were original frames loaded
        /// </summary>
        String FileName();

        /// <summary>
        /// Returns included frames 
        /// </summary>
        List<IPmFrame> GetFrames();

        /// <summary>
        /// Returns included frames count
        /// </summary>
        int Count();

        /// <summary>
        /// TimeStamp of first included frame.
        /// </summary>
        Nullable<DateTime> FirstTimeStamp();

        /// <summary>
        /// Store current included frames to file (type is specified by class instance)
        /// </summary>
        bool CreateOutput(String fileName);

        #endregion

        #region InputMethods

        /// <summary>
        /// Append frames to current Frame Table
        /// </summary>
        void AddFrames(List<IPmFrame> frames);

        /// <summary>
        /// Drop frames current included in Frame Table and set new
        /// </summary>
        void SetFrames(List<IPmFrame> frames);

        #endregion

        #region ControlMethods

        /// <summary>
        /// Open input file and prepare for CreateFrameTable() method
        /// </summary>
        void OpenFile(String filePath);

        /// <summary>
        /// Set input file and prepare for CreateFrameTable() method
        /// </summary>
        void OpenFile(String filePath, BinaryReader binaryReader);

        /// <summary>
        /// Close all opened files (if any) - should be called before reference lost
        /// </summary>
        void Close();

        /// <summary>
        /// Add frames to frame table from current opened file
        /// </summary>
        void CreateFrameTable();

        /// <summary>
        /// Drop frame current included in Frame Table
        /// </summary>
        void ClearFrameTable();

        #endregion

        #region DebuggingMethods

        /// <summary>
        /// Function printing detail content of Frame Vector, purely for debugging purposes
        /// </summary>
        void PrintFramesDetail();

        /// <summary>
        /// Function printing brief content of Frame Vector, purely for debugging purposes
        /// </summary>
        void PrintFramesBrief();

        #endregion

    }
}
