﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2012 Vladimir Vesely (Brno University of Technology - Faculty of information technology)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using System;
using System.Collections.Generic;

namespace PcapMerger
{
    /// <remarks>
    /// Specialized class containing optimized PmHeapSort algorithm for PmFrameVector ADTs
    /// </remarks>
    class PmHeapSort
    {
        public static List<IPmFrame> HeapSort(List<IPmFrame> list)
        {
            for (int i = (list.Count - 1) / 2; i >= 0; i--)
                Adjust(list, i, list.Count - 1);

            for (int i = list.Count - 1; i >= 1; i--)
            {
                var temp = list[0];
                list[0] = list[i];
                list[i] = temp;
                Adjust(list, 0, i - 1);

            }

            return list;
        }

        public static void Adjust(List<IPmFrame> list, int i, int m)
        {
            var temp = list[i];
            int j = i * 2 + 1;
            while (j <= m)
            {
                if (j < m)
                    //if (((IComparable<IPmFrame>)list[j]).CompareTo(list[j + 1]) < 0)
                    if (((IPmFrame)list[j]).CompareTo(list[j + 1]) < 0)
                        j = j + 1;

                //if (((IComparable<IPmFrame>)temp).CompareTo(list[j]) < 0)
                if (((IPmFrame)temp).CompareTo(list[j]) < 0)
                {
                    list[i] = list[j];
                    i = j;
                    j = 2 * i + 1;
                }
                else
                {
                    j = m + 1;
                }
            }
            list[i] = temp;
        }
    }
}
