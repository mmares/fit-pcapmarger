﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PcapMerger
{
    abstract class PmFrameAbstract : IPmFrame
    {
        protected PmSupportedTypes.CaputreFileFrameType cframeType;

        protected BinaryReader sourceFileReader;
        protected DateTime timeStamp;
        protected UInt32 includedLength;
        protected UInt32 originalLength;
        protected UInt32 frameOffset;
        protected PmSupportedTypes.LinkTypes linkType;

        public PmFrameAbstract(BinaryReader srcFile, UInt32 frameOffset, DateTime ts, PmSupportedTypes.LinkTypes lType, UInt32 incLen, UInt32 origLen)
        {
            timeStamp = ts;
            includedLength = incLen;
            originalLength = origLen;
            sourceFileReader = srcFile;
            linkType = lType;
            this.frameOffset = frameOffset;
        }


        /// <summary>
        /// Return link type of original frame
        /// </summary>
        public PmSupportedTypes.LinkTypes LinkType()
        {
            return linkType;
        }

        /// <summary>
        /// Return frame capture time stamp
        /// </summary>
        public DateTime TimeStamp()
        {
            return timeStamp;
        }

        public UInt32 OriginalLength()
        {
            return includedLength;
        }

        public UInt32 IncludedLength()
        {
            return originalLength;
        }

        public UInt32 FrameOffset()
        {
            return frameOffset;
        }

        public PmSupportedTypes.CaputreFileFrameType Type()
        {
            return cframeType;
        }

        /// <summary>
        /// Read raw packet data from source file - MUST BE IMPLEMENTED IN CHILDREN CLASSES
        /// </summary>
        public abstract byte[] Data();


        public int CompareTo(IPmFrame other)
        {
            return other != null ? timeStamp.CompareTo(other.TimeStamp()) : 0;
        }

        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
