﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Net;
using System.Net.NetworkInformation;

namespace PcapMerger
{

    #region PcapNgInterface
    /// <summary>
    /// Represents interface block in PCAPng file
    /// </summary>
    public class PcapNgInterface
    {
        
    /// <summary>
    /// Prints whole TCPD Packet Header inforamtion of one frame record
    /// </summary>
    public enum InterfaceOptions
    { 
        opt_endofopt = 0,
        if_name =	2,
        if_description =	3,
        if_IPv4addr =	4,
        if_IPv6addr	= 5,
        if_MACaddr	= 6,
        if_EUIaddr	= 7,
        if_speed	= 8,
        if_tsresol	= 9,
        if_tzone	= 10,
        if_filter	= 11,
        if_os	= 12,
        if_fcslen	= 13,
        if_tsoffset	= 14
    }

    public PcapNgInterface(UInt16 LinkType, UInt16 id)
    {
        addresses = new List<IPAddress>();
        tsresol = 0;
        this.LinkType = LinkType;
        this.id = id;
        setSnapLenByLinkType();
    }

    public PcapNgInterface(UInt32 SnapLen, UInt16 LinkType, UInt16 id)  :
        this(LinkType,id)
    {
        this.SnapLen = SnapLen;
    }

    void setSnapLenByLinkType()
    {
        SnapLen = 65535; 
    }

    public UInt16 id;
    public UInt32 SnapLen;
    public UInt16 LinkType;
    public String if_name;
    public String if_description;
    public List<IPAddress> addresses;
    public byte[] macAddress;
    public byte[] EUIaddr;
    public UInt64 speed;
    public bool hasTsresol;
    public byte tsresol;
    public UInt64 tzone;
    public byte[] filter;
    public String os;
    public uint fcslen;
    public UInt64 tsoffset;
}

#endregion

    class PmCaptureFilePcapNg : PmCaptureFileAbstract
    {

        #region PcapNgLinkTypes

        /// <summary>
        /// Pcap NG interface Link types
        /// </summary>
        public enum PcapNgLinkType
        { 
            LINKTYPE_NULL   =	0,
            LINKTYPE_ETHERNET   =	1,
            LINKTYPE_EXP_ETHERNET   =	2,
            LINKTYPE_AX25   =	3,
            LINKTYPE_PRONET =	4,
            LINKTYPE_CHAOS  =	5,
            LINKTYPE_TOKEN_RING =	6,
            LINKTYPE_ARCNET	=   7,
            LINKTYPE_SLIP   =	8,
            LINKTYPE_PPP    =	9,
            LINKTYPE_FDDI   =	10,
            LINKTYPE_PPP_HDLC   =	50,
            LINKTYPE_PPP_ETHER  =	51,
            LINKTYPE_SYMANTEC_FIREWALL  =	99,
            LINKTYPE_ATM_RFC1483    =	100,
            LINKTYPE_RAW	=   101,
            LINKTYPE_SLIP_BSDOS =	102,
            LINKTYPE_PPP_BSDOS  =	103,
            LINKTYPE_C_HDLC =	104,
            LINKTYPE_IEEE802_11 =	105,
            LINKTYPE_ATM_CLIP   =	106,
            LINKTYPE_FRELAY =	107,
            LINKTYPE_LOOP   =	108,
            LINKTYPE_ENC    =	109,
            LINKTYPE_LANE8023   =	110,
            LINKTYPE_HIPPI  =	111,
            LINKTYPE_HDLC   =	112,
            LINKTYPE_LINUX_SLL  =	113,
            LINKTYPE_LTALK	=   114,
            LINKTYPE_ECONET	=   115,
            LINKTYPE_IPFILTER   =	116,
            LINKTYPE_PFLOG	=   117,
            LINKTYPE_CISCO_IOS	=   118,
            LINKTYPE_PRISM_HEADER	=   119,
            LINKTYPE_AIRONET_HEADER	=   120,
            LINKTYPE_HHDLC	=   121,
            LINKTYPE_IP_OVER_FC	=   122,
            LINKTYPE_SUNATM	    =   123,
            LINKTYPE_RIO	=   124,
            LINKTYPE_PCI_EXP	=   125,
            LINKTYPE_AURORA	=   126,
            LINKTYPE_IEEE802_11_RADIO	=   127,
            LINKTYPE_TZSP	=   128,
            LINKTYPE_ARCNET_LINUX	=   129,
            LINKTYPE_JUNIPER_MLPPP	=   130,
            LINKTYPE_JUNIPER_MLFR	=   131,
            LINKTYPE_JUNIPER_ES	=   132,
            LINKTYPE_JUNIPER_GGSN	=   133,
            LINKTYPE_JUNIPER_MFR	=   134,
            LINKTYPE_JUNIPER_ATM2	=   135,
            LINKTYPE_JUNIPER_SERVICES	=   136,
            LINKTYPE_JUNIPER_ATM1	=   137,
            LINKTYPE_APPLE_IP_OVER_IEEE1394	=   138,
            LINKTYPE_MTP2_WITH_PHDR	=   139,
            LINKTYPE_MTP2	=   140,
            LINKTYPE_MTP3	=   141,
            LINKTYPE_SCCP	=   142,
            LINKTYPE_DOCSIS	=   143,
            LINKTYPE_LINUX_IRDA	=   144,
            LINKTYPE_IBM_SP	=   145,
            LINKTYPE_IBM_SN	=   146        
        }

        #endregion
        
        #region exceptions
        [Serializable]
        public class PcapNgParsingErrorException : Exception
        {
            public PcapNgParsingErrorException(string message)
                : base(message)
            { }

            protected PcapNgParsingErrorException(SerializationInfo info, StreamingContext ctxt)
                : base(info, ctxt)
            { }
        }

        [Serializable]
        public class PcapNgWriteException : Exception
        {
            public PcapNgWriteException(string message)
                : base(message)
            { }

            protected PcapNgWriteException(SerializationInfo info, StreamingContext ctxt)
                : base(info, ctxt)
            { }
        }
        #endregion

        #region Constructors

        /// <summary>
        /// Basic constructor - generaly for creating output file
        /// </summary>
        public PmCaptureFilePcapNg() :
            base(PmSupportedTypes.CaptureFileTypes.PCAPNextGen)
        {
            ifaces = new List<List<PcapNgInterface>>();
        }

        /// <summary>
        /// Load constructor - generaly for loading input from file
        /// (no need to call OpenFile after using this constructor)
        /// </summary>
        public PmCaptureFilePcapNg(String filePath, BinaryReader fileReader) :
            base(filePath, fileReader, PmSupportedTypes.CaptureFileTypes.PCAPNextGen)
        {
            ifaces = new List<List<PcapNgInterface>>();
        }

        #endregion

        #region PcapNGfileFormatVariables

        List<List<PcapNgInterface>> ifaces; // packets interfaces

        const UInt16 MajorVersion = 0x01;
        const UInt16 MinorVersion = 0x00;

        DateTime unixDateTimeBase = new DateTime(1970, 01, 01, 0, 0, 0);

        #endregion

        #region IPMCaptureFileIOMethods

        /// <summary>
        /// Initialize FrameTables and appropriate FrameVectors from input file
        /// </summary>
        override public void CreateFrameTable()
        {
            try
            {
                ParseFile();
            }
            catch (Exception ex)
            {
                PmConsolePrinter.PrintError("Error>\n" + ex.Message);
            }

        }

        /// <summary>
        /// Store current included frames to file (type is specified by class instance)
        /// </summary>
        override public bool CreateOutput(String outputFile)
        {
            //throw new NotImplementedException("CreateOutput() not implemented !");
            return CreatePcapNgOutput(outputFile);
        }


        #endregion

        #region PcapNgtoSupprotedtypesConversionMethods

        private PmSupportedTypes.LinkTypes convertPcapNgLayer2ToCommonLayer2(PcapNgLinkType linkType)
        {
            switch (linkType)
            {
                case PcapNgLinkType.LINKTYPE_ETHERNET: return PmSupportedTypes.LinkTypes.Ethernet;
                case PcapNgLinkType.LINKTYPE_FDDI: return PmSupportedTypes.LinkTypes.FDDI;
                case PcapNgLinkType.LINKTYPE_RAW: return PmSupportedTypes.LinkTypes.Raw;
                case PcapNgLinkType.LINKTYPE_IEEE802_11: return PmSupportedTypes.LinkTypes.IEEE80211;
                case PcapNgLinkType.LINKTYPE_ATM_RFC1483: return PmSupportedTypes.LinkTypes.ATMRfc1483;
                default: return PmSupportedTypes.LinkTypes.Null;
            }
        }

        private PcapNgLinkType convertCommonLayer2ToPcapNgLayer2(PmSupportedTypes.LinkTypes linkType)
        {
            switch (linkType)
            {
                case PmSupportedTypes.LinkTypes.Ethernet: return PcapNgLinkType.LINKTYPE_ETHERNET;
                case PmSupportedTypes.LinkTypes.FDDI: return PcapNgLinkType.LINKTYPE_FDDI;
                case PmSupportedTypes.LinkTypes.Raw: return PcapNgLinkType.LINKTYPE_RAW;
                case PmSupportedTypes.LinkTypes.IEEE80211: return PcapNgLinkType.LINKTYPE_IEEE802_11;
                case PmSupportedTypes.LinkTypes.ATMRfc1483: return PcapNgLinkType.LINKTYPE_ATM_RFC1483;
                default: return PcapNgLinkType.LINKTYPE_NULL;
            }
        }

        #endregion

        #region PcapNgParsing

        #region ByteOrdeSwapMethods

        /// <summary>
        /// Swap 16-bit number byte order
        /// </summary>
        public static UInt16 swap(UInt16 input)
        {
            return ((UInt16)(((0xFF00 & input) >> 8) | ((0x00FF & input) << 8)));
        }

        /// <summary>
        /// Swap 32-bit number byte order
        /// </summary>
        public static UInt32 swap(UInt32 input)
        {
            return ((UInt32)(((0xFF000000 & input) >> 24) | ((0x00FF0000 & input) >> 8) |
                             ((0x0000FF00 & input) << 8) | ((0x000000FF & input) << 24)));
        }

        /// <summary>
        /// Swap 64-bit number byte order
        /// </summary>
        public static UInt64 swap(UInt64 input)
        {
            return ((0x00000000000000FF) & (input >> 56) | (0x000000000000FF00) & (input >> 40)
                  | (0x0000000000FF0000) & (input >> 24) | (0x00000000FF000000) & (input >> 8)
                  | (0x000000FF00000000) & (input << 8)  | (0x0000FF0000000000) & (input << 24)
                  | (0x00FF000000000000) & (input << 40) | (0xFF00000000000000) & (input << 56));
        }

        #endregion

        /// <summary>
        /// Pcap-ng file blocks types
        /// </summary>
        enum PcapNgBlockType
        {
            SectionHeader,
            InterfaceDescription,
            PacketBlock,
            SimplePacket,
            EnhancedPacket,
            InterfaceStatistics,
            NameResolution,
            Unknown
        }

        #region GeneralBlockParsing

        /// <summary>
        /// Read general block type value
        /// </summary>
        UInt32 GetGeneralBlockType(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset, SeekOrigin.Begin);
            UInt32 blockType = BinReader.ReadUInt32();
            return swapValue ? swap(blockType) : blockType;
        }

        /// <summary>
        /// Read general block length
        /// </summary>
        UInt32 GetGeneralBlockTotalLength(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset+4, SeekOrigin.Begin);
            UInt32 totalLength = BinReader.ReadUInt32();
            return swapValue ? swap(totalLength) : totalLength;
        }

        /// <summary>
        /// Adjust block length to count new offset
        /// </summary>
        UInt32 AdjustLength(UInt32 blockLength)
        {
            switch (blockLength % 4)
            {
                case 0: return blockLength;
                case 1: return blockLength + 3;
                case 2: return blockLength + 2;
                case 3: return blockLength + 1;
                default:
                    return blockLength;
            }
        }

        /// <summary>
        /// Count padding length that had to be added to current data
        /// </summary>
        UInt32 CountPadding(UInt32 blockLength)
        {
            switch (blockLength % 4)
            {
                case 0: return 0;
                case 1: return 3;
                case 2: return 2;
                case 3: return 1;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Count next starting offset behind current block (at current offset)
        /// </summary>
        UInt32 GetNextGeneralBlokOffset(UInt32 offset, bool swapValue)
        {
            return offset + AdjustLength(GetGeneralBlockTotalLength(offset,swapValue));
        }

        /// <summary>
        /// Converts general block value included in file to enumaration type
        /// </summary>
        PcapNgBlockType recognizeGeneralBlock(UInt32 offset, bool swapValue)
        {
            
            UInt32 blockType = GetGeneralBlockType(offset,swapValue);

           // PmConsolePrinter.PrintDebug("Block type\t: " + blockType);

            switch (blockType)
            {
                case 0x0A0D0D0A: return PcapNgBlockType.SectionHeader;
                case 0x00000001: return PcapNgBlockType.InterfaceDescription;
                case 0x00000002: return PcapNgBlockType.PacketBlock;
                case 0x00000003: return PcapNgBlockType.SimplePacket;
                case 0x00000005: return PcapNgBlockType.InterfaceStatistics;
                case 0x00000006: return PcapNgBlockType.EnhancedPacket;
                case 0x00000004: return PcapNgBlockType.NameResolution;                
            }

            return PcapNgBlockType.Unknown;
        }

        #endregion

        #region ParseOptionBlock

        UInt16 GetOptionCode(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset, SeekOrigin.Begin);
            UInt16 optionCode = BinReader.ReadUInt16();
            return swapValue ? swap(optionCode) : optionCode;               
        }

        UInt16 GetOptionLength(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset + 2, SeekOrigin.Begin);
            UInt16 optionLength = BinReader.ReadUInt16();
            return swapValue ? swap(optionLength) : optionLength;
        }

        UInt64 GetOptionUint64(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset, SeekOrigin.Begin);
            UInt64 num = BinReader.ReadUInt64();
            return swapValue ? swap(num) : num;
        }

        byte GetOptionByte(UInt32 offset)
        {
            BinReader.BaseStream.Seek(offset, SeekOrigin.Begin);
            return BinReader.ReadByte();
        }

        String GetOptionString(UInt32 offset, UInt32 length)
        {
            byte[] stringBuffer = new byte[length];
            BinReader.BaseStream.Seek(offset, SeekOrigin.Begin);
            BinReader.Read(stringBuffer, 0, (int)length);
            return Encoding.UTF8.GetString(stringBuffer, 0, (int)length);
        }

        byte[] GetOptionBytes(UInt32 offset, UInt32 length)
        {
            byte[] dataBuffer = new byte[length];
            BinReader.BaseStream.Seek(offset, SeekOrigin.Begin);
            BinReader.Read(dataBuffer, 0, (int)length);
            return dataBuffer;
        }

        #endregion

        #region SectionHeaderBlock

        UInt32 GetByteOrderMagic(UInt32 offset)
        {
            BinReader.BaseStream.Seek(offset + 8, SeekOrigin.Begin);
            return BinReader.ReadUInt32();       
        }


        UInt32 GetVersion(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset + 12, SeekOrigin.Begin);
            UInt16 versionMa = BinReader.ReadUInt16();
            UInt16 versionMi = BinReader.ReadUInt16();

            if (swapValue)
            {
                versionMa = swap(versionMa);
                versionMi = swap(versionMi);
            }

            return (((UInt32)versionMa)<<16) | ((UInt32)versionMi);
        }




        void ParseSectionHeader(UInt32 currentOffset,out bool swapEndian)
        {
            UInt32 byteOrderMagic = GetByteOrderMagic(currentOffset);
            if (byteOrderMagic == 0x4D3C2B1A)
                swapEndian = true;
            else if (byteOrderMagic == 0x1A2B3C4D)
                swapEndian = false;
            else
                throw new PcapNgParsingErrorException("Unknown BYTE ORDER MAGIC VALUE.");

           // Console.WriteLine(GetVersion(currentOffset, false));

            if (GetVersion(currentOffset, false) != 0x010000)
                throw new PcapNgParsingErrorException("Unsupported file version.");
        }

        #endregion

        #region InterfaceDescriptionBlock

        UInt16 GetInterfaceLinkType(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset+8, SeekOrigin.Begin);
            UInt16 linkType = BinReader.ReadUInt16();
            return swapValue ? swap(linkType) : linkType;            
        }

        UInt32 GetInterfaceSnapLen(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset + 12, SeekOrigin.Begin);
            UInt32 snapLen = BinReader.ReadUInt32();
            return swapValue ? swap(snapLen) : snapLen;           
        }

        void ParseInterfaceDescriptionBlock(UInt32 offset, bool swapValue, UInt16 ifaceId, List<PcapNgInterface> sectionIfaceList)
        {
            UInt16 linkType = GetInterfaceLinkType(offset, swapValue);
            UInt32 snapLen = GetInterfaceSnapLen(offset, swapValue);

            PcapNgInterface newIface = new PcapNgInterface(snapLen,linkType,ifaceId);
            sectionIfaceList.Add(newIface);

            UInt32 blockLen = GetGeneralBlockTotalLength(offset, swapValue);

            UInt32 currentOffset = offset+16;
            UInt32 eofBlockOffset = offset + blockLen - 4;

           // PmConsolePrinter.PrintDebug("Interface description block " + currentOffset + " " + eofBlockOffset);

            bool eofFound = false;
            while (!eofFound && currentOffset < eofBlockOffset)
            {
                UInt16 optionCode = GetOptionCode(currentOffset, swapValue);
                UInt16 optionLength = GetOptionLength(currentOffset, swapValue);

                UInt32 dataOffset = currentOffset + 4;

                //Console.WriteLine("Option :" + (PcapNgInterface.InterfaceOptions)optionCode + " " + optionLength + " Lenght : "+ optionLength);
               
                switch ((PcapNgInterface.InterfaceOptions)optionCode)
                {
                    case PcapNgInterface.InterfaceOptions.opt_endofopt      :  
                    eofFound = false;                                                           break;
                    case PcapNgInterface.InterfaceOptions.if_name           : 
                    newIface.if_name = GetOptionString(dataOffset,optionLength);           break;
                    case PcapNgInterface.InterfaceOptions.if_description    :
                    newIface.if_description = GetOptionString(dataOffset,optionLength);    break;
                    case PcapNgInterface.InterfaceOptions.if_IPv4addr       : 
                    newIface.addresses.Add(new IPAddress(GetOptionBytes(dataOffset,optionLength),4)); break;
                    case PcapNgInterface.InterfaceOptions.if_IPv6addr	    : 
                    newIface.addresses.Add(new IPAddress(GetOptionBytes(dataOffset,optionLength),4)); break;
                    case PcapNgInterface.InterfaceOptions.if_MACaddr	    :
                    newIface.macAddress =  GetOptionBytes(dataOffset,optionLength);        break;
                    case PcapNgInterface.InterfaceOptions.if_EUIaddr	    :
                    newIface.EUIaddr =  GetOptionBytes(dataOffset,optionLength);           break;
                    case PcapNgInterface.InterfaceOptions.if_speed	        :
                    newIface.speed = GetOptionUint64(dataOffset,swapValue);                break;
                    case PcapNgInterface.InterfaceOptions.if_tsresol	    :                   
                    newIface.tsresol = GetOptionByte(dataOffset);
                    newIface.hasTsresol = true;                                            break;
                    case PcapNgInterface.InterfaceOptions.if_tzone	        :
                    newIface.tzone = GetOptionUint64(dataOffset,swapValue);                break;
                    case PcapNgInterface.InterfaceOptions.if_filter	        :
                    newIface.filter =  GetOptionBytes(dataOffset,optionLength);            break;
                    case PcapNgInterface.InterfaceOptions.if_os	            :
                    newIface.os = GetOptionString(dataOffset,optionLength);                break;
                    case PcapNgInterface.InterfaceOptions.if_fcslen	        : 
                    newIface.fcslen = GetOptionByte(dataOffset);                           break;
                    case PcapNgInterface.InterfaceOptions.if_tsoffset       : 
                    newIface.tsoffset = GetOptionUint64(dataOffset,swapValue);             break;
                }

                currentOffset += AdjustLength(optionLength)+4;
            }

            
        }

        #endregion

        DateTime ConvertUint64ToTimeStamp(UInt64 data, PcapNgInterface iface)
        {
            ulong mult;
            if (iface.hasTsresol)
            {
                byte tsresol = iface.tsresol;
                int pow = (tsresol & 0x7F);
                if ((tsresol & 0x80) == 0)      // negPower of 10
                    mult = (ulong)Math.Pow(10, pow);
                else  // negPower of 2
                    mult = (ulong)Math.Pow(2, pow);
            }
            else
                mult = 1000000;

            UInt64 sec =  (UInt64)(data / mult);
            ulong frac = (ulong)(data % mult);
            sec += iface.tzone;

            UInt32 ticksPerSecond = 10000000;

            Double fracMult = ticksPerSecond / mult;

          //  Console.WriteLine("Ticks per second : " + DateTime.TicksPerSecond);
            

            DateTime pTime = new DateTime(1970, 01, 01, 0, 0, 0);
            pTime = pTime.AddSeconds(sec);
            pTime = pTime.AddTicks((long)(frac*fracMult));
            return pTime;
        }

        #region ParseEnhancedPacketBlock

        UInt32 GetEhcPacketBlockInterfaceID(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset + 8, SeekOrigin.Begin);
            UInt32 InterfaceID = BinReader.ReadUInt32();
            return swapValue ? swap(InterfaceID) : InterfaceID;             
        }

        UInt32 GetEhcPacketBlockTimestampH(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset + 12, SeekOrigin.Begin);
            UInt32 value = BinReader.ReadUInt32();
            return swapValue ? swap(value) : value;
        }

        UInt32 GetEhcPacketBlockTimestampL(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset + 16, SeekOrigin.Begin);
            UInt32 value = BinReader.ReadUInt32();
            return swapValue ? swap(value) : value;
        }

        UInt32 GetEhcPacketBlockCapturedLen(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset + 20, SeekOrigin.Begin);
            UInt32 CapturedLen = BinReader.ReadUInt32();
            return swapValue ? swap(CapturedLen) : CapturedLen;
        }

        UInt32 GetEhcPacketBlockPacketLen(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset + 24, SeekOrigin.Begin);
            UInt32 PacketLen = BinReader.ReadUInt32();
            return swapValue ? swap(PacketLen) : PacketLen;
        }

        enum EnhancedPacketBlockOptions
        {
            epb_flags = 2,
            epb_hash = 3,
            epb_dropcount = 4
        }

        void ParseEnhancedPacketBlock(UInt32 offset, bool swapValue, List<PcapNgInterface> sectionIfaceList)
        {
            //PmConsolePrinter.PrintDebug("Enhanced Packet Block " + offset);
            UInt32 ifaceID = GetEhcPacketBlockInterfaceID(offset, swapValue);

            if (sectionIfaceList.Count() <= ifaceID || sectionIfaceList[(int)ifaceID].id != ifaceID)
            {
                throw new PcapNgParsingErrorException("No such an interface !");
            }

            PcapNgInterface iface = sectionIfaceList[(int)ifaceID];

            UInt64 timeStamp = (UInt64)GetEhcPacketBlockTimestampH(offset, swapValue) << 32 |
                               GetEhcPacketBlockTimestampL(offset, swapValue);
            UInt32 capturedLen = GetEhcPacketBlockCapturedLen(offset, swapValue);
            UInt32 packetLen = GetEhcPacketBlockPacketLen(offset, swapValue);

            UInt32 blockLen = GetGeneralBlockTotalLength(offset, swapValue);

            DateTime packetTimeStamp = ConvertUint64ToTimeStamp(timeStamp,iface);

            PmFramePcapNg  newFrame = new PmFramePcapNg(    BinReader,
                                                            offset,
                                                            packetTimeStamp,
                                                            convertPcapNgLayer2ToCommonLayer2((PcapNgLinkType)iface.LinkType),
                                                            capturedLen,
                                                            packetLen,
                                                            PmFramePcapNg.FrameBLockType.EnhancedPacket,
                                                            iface
                                                        );

            UInt32 currentOffset = offset + 28 + AdjustLength(capturedLen);
            UInt32 eofBlockOffset = offset + blockLen - 4;

            bool eofFound = false;
            while (!eofFound && currentOffset < eofBlockOffset)
            {
                UInt16 optionCode = GetOptionCode(currentOffset, swapValue);
                UInt16 optionLength = GetOptionLength(currentOffset, swapValue);

                UInt32 dataOffset = currentOffset + 4;

                switch ((EnhancedPacketBlockOptions)optionCode)
                {
                    case EnhancedPacketBlockOptions.epb_flags: 
                        newFrame.EpbFlags = GetOptionBytes(dataOffset,4);   break;
                    case EnhancedPacketBlockOptions.epb_hash:
                        newFrame.EpbHash = GetOptionBytes(dataOffset, optionLength); break;
                    case EnhancedPacketBlockOptions.epb_dropcount:
                        newFrame.Dropcount = GetOptionUint64(dataOffset, swapValue); break;
                }

                currentOffset += AdjustLength(optionLength) + 4;

            }

            Frames.Add(newFrame);
                        
        }

        #endregion

        #region ParseSimplyPacketBlock


        UInt32 GetSimplyPacketBlockPacketLen(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset + 8, SeekOrigin.Begin);
            UInt32 PacketLen = BinReader.ReadUInt32();
            return swapValue ? swap(PacketLen) : PacketLen;
        }

        

        void ParseSimplyPacketBlock(UInt32 offset, bool swapValue, List<PcapNgInterface> sectionIfaceList)
        {
            //PmConsolePrinter.PrintDebug("Simply Packet Block " + offset);

            if (!sectionIfaceList.Any())
            {
                throw new PcapNgParsingErrorException("No interface for this packet !");
            }

            UInt32 packetLen = GetSimplyPacketBlockPacketLen(offset, swapValue);

            DateTime packetTimeStamp = new DateTime(0,0,0,0,0,0);

            PmFramePcapNg newFrame = new PmFramePcapNg(BinReader,
                                                            offset,
                                                            packetTimeStamp,
                                                            convertPcapNgLayer2ToCommonLayer2((PcapNgLinkType)sectionIfaceList.First().LinkType),
                                                            packetLen,
                                                            packetLen,
                                                            PmFramePcapNg.FrameBLockType.SimplePacket,
                                                            sectionIfaceList.First()
                                                        );
            Frames.Add(newFrame);
        }

        #endregion

        #region ParsePacketBlock(Obsolete)

        UInt16 GetPacketBlockInterfaceID(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset + 8, SeekOrigin.Begin);
            UInt16 InterfaceID = BinReader.ReadUInt16();
            return swapValue ? swap(InterfaceID) : InterfaceID;
        }

        UInt16 GetPacketBlockDropsCount(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset + 10, SeekOrigin.Begin);
            UInt16 InterfaceID = BinReader.ReadUInt16();
            return swapValue ? swap(InterfaceID) : InterfaceID;
        }

        UInt32 GetPacketBlockTimestampH(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset + 12, SeekOrigin.Begin);
            UInt32 value = BinReader.ReadUInt32();
            return swapValue ? swap(value) : value;
        }

        UInt32 GetPacketBlockTimestampL(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset + 16, SeekOrigin.Begin);
            UInt32 value = BinReader.ReadUInt32();
            return swapValue ? swap(value) : value;
        }

        UInt32 GetPacketBlockCapturedLen(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset + 20, SeekOrigin.Begin);
            UInt32 CapturedLen = BinReader.ReadUInt32();
            return swapValue ? swap(CapturedLen) : CapturedLen;
        }

        UInt32 GetPacketBlockPacketLen(UInt32 offset, bool swapValue)
        {
            BinReader.BaseStream.Seek(offset + 24, SeekOrigin.Begin);
            UInt32 PacketLen = BinReader.ReadUInt32();
            return swapValue ? swap(PacketLen) : PacketLen;
        }

        void ParsePacketBlock(UInt32 offset, bool swapValue, List<PcapNgInterface> sectionIfaceList)
        {
            UInt16 interfaceID = GetPacketBlockInterfaceID(offset, swapValue);

            if (sectionIfaceList.Count() <= interfaceID || sectionIfaceList[(int)interfaceID].id != interfaceID)
            {
                throw new PcapNgParsingErrorException("No such an interface !");
            }

            PcapNgInterface iface = sectionIfaceList[(int)interfaceID];
            UInt16 dropCount = GetPacketBlockDropsCount(offset, swapValue);

            UInt64 timeStamp = (UInt64)GetPacketBlockTimestampH(offset, swapValue) << 32 |
                                       GetPacketBlockTimestampL(offset, swapValue);
            UInt32 capturedLen = GetPacketBlockCapturedLen(offset, swapValue);
            UInt32 packetLen = GetPacketBlockPacketLen(offset, swapValue);

            UInt32 blockLen = GetGeneralBlockTotalLength(offset, swapValue);

            DateTime packetTimeStamp = ConvertUint64ToTimeStamp(timeStamp, iface);
            //DateTime packetTimeStamp = DateTime.Now;

            PmFramePcapNg newFrame = new PmFramePcapNg(BinReader,
                                                        offset,
                                                        packetTimeStamp,
                                                        convertPcapNgLayer2ToCommonLayer2((PcapNgLinkType)iface.LinkType),
                                                        capturedLen,
                                                        packetLen,
                                                        PmFramePcapNg.FrameBLockType.PacketBlock,
                                                        iface
                                                        );

            Frames.Add(newFrame);

        }

        #endregion

        void ParseFile()
        {
            UInt32 currentOffset = 0;
            uint blockCounter = 0;

            PcapNgBlockType currentBlockType = PcapNgBlockType.Unknown;

            var fileSize = BinReader.BaseStream.Length;
            bool swapEndian = false;

            UInt16 ifaceIdCounter = 0;

            List<PcapNgInterface> sectionIfaceList = null;

            int unknownBlocksCount = 0;

            while (currentOffset < fileSize)
            {
                currentBlockType = recognizeGeneralBlock(currentOffset, swapEndian);
                blockCounter++;
               /* PmConsolePrinter.PrintDebug("Block type\t: " + currentBlockType);
                PmConsolePrinter.PrintDebug("Current offset\t: " + currentOffset.ToString());
                PmConsolePrinter.PrintDebug("Total Length\t: " + (GetNextGeneralBlokOffset(currentOffset, swapEndian) - currentOffset).ToString());
                PmConsolePrinter.PrintDebug("Block No.\t: " + blockCounter.ToString());
                PmConsolePrinter.PrintDebugDelimiter();*/

                switch (currentBlockType)
                {
                    case PcapNgBlockType.SectionHeader          : 
                                ParseSectionHeader(currentOffset,out swapEndian);
                                ifaceIdCounter = 0; 
                                sectionIfaceList = new List<PcapNgInterface>();
                                ifaces.Add(sectionIfaceList); 
                                break;
                    case PcapNgBlockType.InterfaceDescription   :
                                ParseInterfaceDescriptionBlock(currentOffset, swapEndian, ifaceIdCounter, sectionIfaceList);
                                ifaceIdCounter++; 
                                break;
                    case PcapNgBlockType.EnhancedPacket         :
                                ParseEnhancedPacketBlock(currentOffset, swapEndian, sectionIfaceList);
                                break;
                    case PcapNgBlockType.SimplePacket           :
                                ParseSimplyPacketBlock(currentOffset, swapEndian, sectionIfaceList);
                                break;
                    case PcapNgBlockType.PacketBlock            :
                                ParsePacketBlock(currentOffset, swapEndian, sectionIfaceList);
                                break;
                }

                UInt32 newOffset = GetNextGeneralBlokOffset(currentOffset, swapEndian);

                if (currentOffset == newOffset)
                {
                    throw new PcapNgParsingErrorException("Broken file ?");
                }

                currentOffset = newOffset;

                if (currentBlockType == PcapNgBlockType.Unknown)
                    unknownBlocksCount++;
            }

            if (unknownBlocksCount > 0)
                throw new PcapNgParsingErrorException( unknownBlocksCount.ToString()  + " unknown blocks where skipped ...");
        }

        #endregion

        #region PcapNgOutput

        /// <summary>
        /// Count section header block size without any options (in bytes)
        /// </summary>
        UInt32 CountSectionHeaderBlockSize()
        {
            UInt32 SectionHeaderSize = 0;

            SectionHeaderSize += 4; // Block type
            SectionHeaderSize += 4; // Block total length
            SectionHeaderSize += 4; // Byte Order magic
            SectionHeaderSize += 2; // Major version
            SectionHeaderSize += 2; // Minor- version
            SectionHeaderSize += 8; // Section length
            SectionHeaderSize += 4; // Block total length

            return SectionHeaderSize;
        }

        /// <summary>
        /// Write section header block without any options to output file stream
        /// </summary>
        UInt32 WriteSectionHeaderBlock(BinaryWriter BinWriter, UInt64 sectionSize)
        {
            UInt32 blockTotalLength = CountSectionHeaderBlockSize();
            BinWriter.Write((UInt32)0x0A0D0D0A);
            BinWriter.Write((UInt32)blockTotalLength);
            BinWriter.Write((UInt32)0x1A2B3C4D);
            BinWriter.Write(MajorVersion);
            BinWriter.Write(MinorVersion);
            BinWriter.Write(sectionSize);
            BinWriter.Write((UInt32)blockTotalLength);

            return blockTotalLength;
        }

        /// <summary>
        /// Write section header block without any options to output file stream
        /// Can be used if section size is not known
        /// </summary>
        UInt32 WriteSectionHeaderBlock(BinaryWriter BinWriter)
        {
            return WriteSectionHeaderBlock(BinWriter, (UInt64)0xFFFFFFFFFFFFFFFF);
        }

        /// <summary>
        /// Count size of interface description block with one option (tsresol)
        /// </summary>
        UInt32 CountInterfaceDescriptionBlockSize()
        {
            UInt32 InterfaceDescriptionBlockSize = 0;

            InterfaceDescriptionBlockSize += 4; // Block type
            InterfaceDescriptionBlockSize += 4; // Block total length

            InterfaceDescriptionBlockSize += 2; // LinkType
            InterfaceDescriptionBlockSize += 2; // Reserved

            InterfaceDescriptionBlockSize += 4; // SnapLen

            InterfaceDescriptionBlockSize += 8; // Option tsresol
            InterfaceDescriptionBlockSize += 4; // Option opt_endofopt

            InterfaceDescriptionBlockSize += 4; // Block total length

            return InterfaceDescriptionBlockSize;
        }

        /// <summary>
        /// Convert DataTime stamp to value in nanoseconds (can be used in output)
        /// </summary>
        UInt64 ConvertTimeStampToUint64(DateTime timeStamp)
        {
            TimeSpan diff = timeStamp - unixDateTimeBase;
            return (UInt64)diff.Ticks*100;
        }

        /// <summary>
        /// Write interface description block to output file based on specified interface instance
        /// </summary>
        UInt32 WriteInterfaceDescriptionBlock(BinaryWriter BinWriter, PcapNgInterface iface)
        {
            UInt32 blockTotalLength = CountInterfaceDescriptionBlockSize();
            BinWriter.Write((UInt32)0x00000001);        // Block type
            BinWriter.Write((UInt32)blockTotalLength);  // Block total length
            BinWriter.Write((UInt16)iface.LinkType);    // LinkType
            BinWriter.Write((UInt16)0x0000);            // reserved
            BinWriter.Write((UInt32)iface.SnapLen);

            // Option tsresol :
            BinWriter.Write((UInt16)PcapNgInterface.InterfaceOptions.if_tsresol);
            BinWriter.Write((UInt16)0x0001);

            BinWriter.Write((byte)0x09);    // in nanoseconds
            BinWriter.Write((byte)0x00);    // padding
            BinWriter.Write((byte)0x00);    // padding
            BinWriter.Write((byte)0x00);    // padding


            // Option opt_endofopt :
            BinWriter.Write((UInt32)0x0000);

            BinWriter.Write((UInt32)blockTotalLength);// Block total length

            return blockTotalLength;
        }

        /// <summary>
        /// Count size (in bytes) of Enhanced packet block without packet data and any options
        /// </summary>
        UInt32 CountEnhancedPacketBlockBlockSize()
        {
            UInt32 EnhancedPacketBlockBlockSize = 0;

            EnhancedPacketBlockBlockSize += 4; // Block type
            EnhancedPacketBlockBlockSize += 4; // Block total length
            EnhancedPacketBlockBlockSize += 4; // InterfaceID  
            EnhancedPacketBlockBlockSize += 4; // Timestamp(High)  
            EnhancedPacketBlockBlockSize += 4; // Timestamp(Low)  
            EnhancedPacketBlockBlockSize += 4; // CapturedLen  
            EnhancedPacketBlockBlockSize += 4; // PacketLen 

            EnhancedPacketBlockBlockSize += 4; // Block total length

            return EnhancedPacketBlockBlockSize;
        }

        /// <summary>
        /// Write Enhanced pcket block to output file based on specified frame.
        /// Interface will one of the virtual interfaces from vIfaces dictionary
        /// according to frame link type.
        /// </summary>
        UInt32 WriteEnhancedPacketBlock(BinaryWriter BinWriter, IPmFrame frame, Dictionary<PmSupportedTypes.LinkTypes, PcapNgInterface> vIfaces)
        {
            byte[] frameData = frame.Data();
            if (frameData == null)
                return 0;

            UInt32 enhancedPacketBlockLength = CountEnhancedPacketBlockBlockSize() + (UInt32)frameData.Length;

            PcapNgInterface vIface = vIfaces[frame.LinkType()];

            BinWriter.Write((UInt32)0x00000006);
            BinWriter.Write((UInt32)enhancedPacketBlockLength);
            BinWriter.Write((UInt32)vIface.id);

            UInt64 timeStampValue = ConvertTimeStampToUint64(frame.TimeStamp());

            BinWriter.Write((UInt32)(timeStampValue>>32));
            BinWriter.Write((UInt32)(timeStampValue&0xFFFFFFFF));

            BinWriter.Write((UInt32)frame.IncludedLength());
            BinWriter.Write((UInt32)frame.OriginalLength());

            BinWriter.Write(frameData);

            uint paddingLen = CountPadding((UInt32)frameData.Length);

            for (uint i = 0; i < paddingLen; i++)
                BinWriter.Write((byte)0x00);           

            BinWriter.Write((UInt32)enhancedPacketBlockLength);

            return enhancedPacketBlockLength + paddingLen;
        }


        /// <summary>
        /// Create PCAP-ng file that includes all frames stored in current instace.
        /// For each link type will be created "virtual interface" and all frames 
        /// with this link type will in output file belong to it.
        /// </summary>
        bool CreatePcapNgOutput(String outputFile)
        {
            try
            {
                // holds list of all link types inclede in frame vector
                List<PmSupportedTypes.LinkTypes> framesLinkTypes = new List<PmSupportedTypes.LinkTypes>();

                // TODO - optimize LINQ ?
                foreach (var fr in Frames)
                {
                    if (!framesLinkTypes.Contains(fr.LinkType()))
                        framesLinkTypes.Add(fr.LinkType());
                }

                // holds list of new created virtual interfaces :
                List<PcapNgInterface> virtualInterfaces = new List<PcapNgInterface>();
                // directory for faster lookup :
                var virtualInterfacesDictionary = new Dictionary<PmSupportedTypes.LinkTypes, PcapNgInterface>();

                UInt16 ifaceId = 0;
                foreach (var type in framesLinkTypes)
                {   // create new itnerface for each link type :
                    PcapNgInterface newIface = new PcapNgInterface((UInt16)convertCommonLayer2ToPcapNgLayer2(type), ifaceId);
                    virtualInterfaces.Add(newIface);
                    virtualInterfacesDictionary.Add(type, newIface);
                    ifaceId++;
                }

                // open output file stream :
                BinaryWriter BinWriter = new BinaryWriter(File.Open(Path.GetFullPath(outputFile), FileMode.Create));

                // skipt section header for now - will be add at end
                BinWriter.BaseStream.Seek(CountSectionHeaderBlockSize(), SeekOrigin.Begin);
                UInt64 sectionSize = 0;

                foreach (var iface in virtualInterfaces)
                {   // write blocks of all new virtual interfaces
                    sectionSize += WriteInterfaceDescriptionBlock(BinWriter, iface);
                }

                foreach (var fr in Frames)
                {   // write blocks for alle frames in frame vector
                    sectionSize += WriteEnhancedPacketBlock(BinWriter, fr, virtualInterfacesDictionary);
                }

                // add section header size (section size is now known) :
                BinWriter.BaseStream.Seek(0, SeekOrigin.Begin);
                WriteSectionHeaderBlock(BinWriter, sectionSize);

                // close output file
                BinWriter.Close();
            }
            /* If anything went bad generate exception and return false */
            catch (Exception ex)
            {
                PmConsolePrinter.PrintError(ex.Message);
                return false;
            }
            /* otherwise return true if everything went good */
            return true;
        }

        #endregion

        #region PcapNgDebugPrints

        String MacAddressToString(byte[] address)
        {
            String result = "";
            if (address != null && address.Length == 6)
            {
                for (int i = 0; i < address.Length; i++)
                {
                    // Display the physical address in hexadecimal.
                    result += address[i].ToString("X2");
                    // Insert a hyphen after each byte, unless we are at the end of the 
                    // address.
                    if (i != address.Length - 1)
                    {
                        result += "-";
                    }
                }
            }

            return result;
        }

        public void PrintInterfacesList()
        {
            PmConsolePrinter.PrintDebug("PCAP-NG INTERFACES FOR FILE " + Path.GetFileName(FilePath));

            int sectionId = 0;
            foreach (var section in ifaces)
            {
                PmConsolePrinter.PrintDebug("SECTION #"+ sectionId);
                foreach (var iface in section)
                {
                    PmConsolePrinter.PrintInfoEol("ID> {0}", iface.id);
                    PmConsolePrinter.PrintInfoEol("SNAPLEN> {0}", iface.SnapLen);
                    PmConsolePrinter.PrintInfoEol("LINKTYPE> {0} \"{1}\"",iface.LinkType, (PcapNgLinkType)iface.LinkType);
                    PmConsolePrinter.PrintInfoEol("IF NAME> \"{0}\"", iface.if_name);
                    PmConsolePrinter.PrintInfoEol("IF DESCRIPTION> \"{0}\"", iface.if_description);
                    PmConsolePrinter.PrintInfoEol("ADDRESSES> ");
                        foreach (var address in iface.addresses)
                            PmConsolePrinter.PrintInfoEol("\t\t" + address.ToString());
                    PmConsolePrinter.PrintInfoEol("MAC ADDRESSES> "+MacAddressToString(iface.macAddress));
                    PmConsolePrinter.PrintInfoEol("SPEED> {0}", iface.speed);
                    PmConsolePrinter.PrintInfoEol("OS> \"{0}\"", iface.os);
                }
                sectionId++;
            }
        }

        #endregion

    }
}
