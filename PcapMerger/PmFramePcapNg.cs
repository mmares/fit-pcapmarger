﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PcapMerger
{
    class PmFramePcapNg : PmFrameAbstract
    {

        /// <summary>
        /// Block type must be known to get raw packet data
        /// </summary>
        internal enum FrameBLockType
        {
            PacketBlock,
            SimplePacket,
            EnhancedPacket
        }

        FrameBLockType blockType;   // holds this packet data
        PcapNgInterface iface;      // holds link to interface to which frame belongs

        // optional values :
        byte[] epb_flags;
        byte[] epb_hash;
        UInt64 dropcount;

        public byte[] EpbFlags
        {
            get { return epb_flags; }
            set { epb_flags = value; }
        }

        public byte[] EpbHash
        {
            get { return epb_hash; }
            set { epb_hash = value; }
        }

        public UInt64 Dropcount
        {
            get { return dropcount; }
            set { dropcount = value; }
        }

        public PmFramePcapNg(BinaryReader srcFile, UInt32 frameOffset, DateTime ts, PmSupportedTypes.LinkTypes lType, UInt32 incLen, UInt32 origLen, FrameBLockType type, PcapNgInterface iface) :
            base (srcFile,frameOffset, ts, lType,incLen,origLen)
        {
            blockType = type;
            this.iface = iface;
            cframeType = PmSupportedTypes.CaputreFileFrameType.PcapNg;
        }


        /// <summary>
        /// Read raw packet data from source file
        /// </summary>
        public override byte[] Data()
        {
            if (sourceFileReader == null)
                return null;

            UInt32 startOffset = 0;

            switch (blockType)
            { 
                case FrameBLockType.EnhancedPacket :
                startOffset = 28; break;
                case FrameBLockType.SimplePacket:
                startOffset = 12; break;
                case FrameBLockType.PacketBlock :
                startOffset = 28; break;
            }

            if (startOffset == 0)
                return null;

            sourceFileReader.BaseStream.Seek(frameOffset + startOffset, SeekOrigin.Begin);
            return sourceFileReader.ReadBytes((int)includedLength);
        }
    }
}
