﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2012 Vladimir Vesely (Brno University of Technology - Faculty of information technology)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;

namespace PcapMerger
{
    class PmCaptureFilesManager
    {

        #region exceptions
        [Serializable]
        public class UnknownFileType : Exception
        {
            public UnknownFileType(string message) 
                : base(message)
            {}

            protected UnknownFileType(SerializationInfo info, StreamingContext ctxt)
                : base(info, ctxt)
            { }
        }

        [Serializable]
        public class NotSuportedFileException : Exception
        {
            public NotSuportedFileException(string message)
                : base(message)
            { }

            protected NotSuportedFileException(SerializationInfo info, StreamingContext ctxt)
                : base(info, ctxt)
            { }
        }
        #endregion

        static private PmSupportedTypes.CaptureFileTypes DetermineType(BinaryReader fileReader)
        {
            if (fileReader == null)
                throw new System.ArgumentException("Parameter cannot be null", "fileReader");

            fileReader.BaseStream.Seek(0, SeekOrigin.Begin);

            var signature = fileReader.ReadUInt32();
            //var buf = new byte[4];
            //sfs.BinReader.BaseStream.Seek(0, SeekOrigin.Begin);
            //sfs.BinReader.BaseStream.Read(buf, 0, 4);
            //PrintDebug(buf[0] + "x" + buf[1] + "x" + buf[2] + "x" + buf[3] + "x");
            //Console.WriteLine("{0:X}", signatura);       
            switch (signature)
            {
                /* Begins with GMBU which is significant for MNM */
                case 0x55424d47:
                case 0x474d4255:
                    return PmSupportedTypes.CaptureFileTypes.MicrosoftNetworkMonitor;
                /* Begins with magic number 0xA1B2C3D4 or 0x4D3C2B1A significant for TCPDump/LibPCAP ver 2.3*/
                case 0x4d3c2b1a:
                case 0xa1b2c3d4:
                    return PmSupportedTypes.CaptureFileTypes.LibPCAP;
                case 0x0A0D0D0A:
                    return PmSupportedTypes.CaptureFileTypes.PCAPNextGen;
            }
            return PmSupportedTypes.CaptureFileTypes.Unknown;
        }

        static public IPmCaptureFile createCaptureFile(PmSupportedTypes.CaptureFileTypes fileType)
        {
            switch (fileType)
            {
                case PmSupportedTypes.CaptureFileTypes.MicrosoftNetworkMonitor:

                    return new PmCaptureFileMNM();

                case PmSupportedTypes.CaptureFileTypes.LibPCAP:

                    return new PmCaptureFilePcap();

                case PmSupportedTypes.CaptureFileTypes.PCAPNextGen:

                    return new PmCaptureFilePcapNg();

                default: break;
            };

            return null;
        }

        static public IPmCaptureFile createCaptureFile(String filePath)
        {
            BinaryReader fileBinaryReader = new BinaryReader(new FileStream(filePath, FileMode.Open));
            PmSupportedTypes.CaptureFileTypes fileType = DetermineType(fileBinaryReader);

            if (fileType == PmSupportedTypes.CaptureFileTypes.Unknown)
                throw new UnknownFileType("Capture file format can not be determined for " + Path.GetFileName(filePath));

            IPmCaptureFile captureFile = createCaptureFile(fileType);

            switch (fileType)
            {
                case PmSupportedTypes.CaptureFileTypes.MicrosoftNetworkMonitor:

                    return new PmCaptureFileMNM(filePath, fileBinaryReader);

                case PmSupportedTypes.CaptureFileTypes.LibPCAP:

                    return new PmCaptureFilePcap(filePath, fileBinaryReader);

                case PmSupportedTypes.CaptureFileTypes.PCAPNextGen:

                    return new PmCaptureFilePcapNg(filePath, fileBinaryReader);

                default: break;
            };

            fileBinaryReader.Close();
            throw new NotSuportedFileException("This file type is not supported. File : " + Path.GetFileName(filePath));
        }


        /// <summary>
        /// Creates one giant collection filled will all frames sorted according to timestamp information
        /// </summary>
        /// <param name="sfsList">Collection of Sniff File Specific ADTs</param>
        /// <param name="sort">Used sorting algorithm, currently supports heapsort and quicksort</param>
        /// <returns>Returns collection containing frames from all Sniff File Specification ADTs past as input</returns>        
        public static List<IPmFrame> SortFramesIntoFV(IEnumerable<IPmCaptureFile> sfsList, String sort)
        {
            /* Create auxiliary collection for sorting */
            var frames = new List<IPmFrame>();
            foreach (var spec in sfsList)
                frames.AddRange(spec.GetFrames());

            /* Follows two sorting options */

            switch (sort.ToLower())
            {
                /* Either use HeapSort */
                case "heapsort":
                    return PmHeapSort.HeapSort(frames);
                /* Or use standard optimized QuickSort */
                default:
                case "quicksort":
                    frames.Sort((fr1, fr2) => fr2 != null ? fr1.TimeStamp().CompareTo(fr2.TimeStamp()) : 0);
                    return frames;
                case "mergesort":
                    return PmInPlaceMergeSort.MergeSort(frames);
            }
        }


    }
}
