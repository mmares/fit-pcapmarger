﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PcapMerger
{
    class PmFrameMNM : PmFrameAbstract
    {
        public PmFrameMNM(BinaryReader srcFile, UInt32 frameOffset, DateTime ts, PmSupportedTypes.LinkTypes lType, UInt32 incLen, UInt32 origLen) :
            base (srcFile, frameOffset, ts, lType,incLen,origLen)
        {
            cframeType = PmSupportedTypes.CaputreFileFrameType.MNM;
        }

        /// <summary>
        /// Read raw packet data from source file
        /// </summary>
        public override byte[] Data()
        {
            if (sourceFileReader == null)
                return null;

            sourceFileReader.BaseStream.Seek(frameOffset + 16, SeekOrigin.Begin);
            return sourceFileReader.ReadBytes((int)includedLength);
        }
    }
}
