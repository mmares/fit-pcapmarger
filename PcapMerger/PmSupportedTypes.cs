﻿/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2012 Vladimir Vesely (Brno University of Technology - Faculty of information technology)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PcapMerger
{
    /// <summary>
    /// Class providing definition of supported file and link layer types
    /// </summary>
    internal class PmSupportedTypes
    {

        /// <summary>
        /// Capture file formats spported by this app
        /// </summary>
        internal enum CaptureFileTypes
        {
            MicrosoftNetworkMonitor,
            LibPCAP,
            PCAPNextGen,
            Unknown
        }

        internal enum CaputreFileFrameType
        { 
            Pcap,
            PcapNg,
            MNM,          
        }

        /// <summary>
        /// Link types supproted by this app
        /// </summary>
        internal enum LinkTypes
        {
            Ethernet,
            FDDI,
            Raw,
            IEEE80211,
            ATMRfc1483,
            Null
        }

        /// <summary>
        /// Pragma function reeturning if file type is supproted by this app
        /// </summary>
        /// <param name="type">Capture file type</param>
        /// <returns>If type is supported then returns true otherwise false</returns>
        internal static bool IsSupportedLinkType(LinkTypes linkType)
        {
            switch (linkType)
            {
                case LinkTypes.Ethernet:
                case LinkTypes.FDDI:
                case LinkTypes.Raw:
                case LinkTypes.IEEE80211:
                case LinkTypes.ATMRfc1483:
                    return true;
                default:
                    return false;
            }
        }
    }
}
